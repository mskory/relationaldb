package com.mskory.dao;

import com.mskory.models.Product;
import com.mskory.models.Store;
import com.mskory.util.ConnectionPool;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class StoreDaoImplTest {
    static ConnectionPool connectionPool = Mockito.mock();
    static Connection connection = Mockito.mock();
    static PreparedStatement statement = Mockito.mock();

    @BeforeAll
    static void beforeAll() throws SQLException {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
    }

    @Test
    void addFromCSV() throws SQLException {
        StoreDaoImpl storeDao = new StoreDaoImpl(connectionPool);
        storeDao.addFromCSV("stores.csv");
        verify(statement, times(15)).addBatch();
        verify(statement).executeBatch();
        verify(connection).commit();
        verify(connectionPool).releaseConnection(connection);
    }
}