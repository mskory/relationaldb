package com.mskory.dao;

import com.mskory.models.Product;
import com.mskory.util.ConnectionPool;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class StockDaoImplTest {
    static ConnectionPool connectionPool = Mockito.mock();
    static Connection connection = Mockito.mock();
    static PreparedStatement  statement = Mockito.mock();
    static ResultSet resultSet = Mockito.mock();
    StockDaoImpl stockDao = new StockDaoImpl(connectionPool);
    List<Product> products =  List.of(new Product().setName("name"));

    @BeforeAll
    static void beforeAll() throws SQLException {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
        when(statement.executeQuery()).thenReturn(resultSet);
    }

    @Test
    void addListMethod_Ok() throws SQLException {
        when(resultSet.next()).thenReturn(true,  false);
        List<Product> returnedProducts = stockDao.addList(products);
        verify(statement, times(1)).addBatch();
        verify(statement, times(1)).executeBatch();
        verify(connection, times(1)).commit();
        verify(statement, times(1)).execute();
        verify(connectionPool, times(2)).releaseConnection(connection);
        assertEquals(products.get(0), returnedProducts.get(0));
    }

    @Test
    void printStoreWithMaxProductsInCategory_Ok() throws SQLException {
        when(resultSet.next()).thenReturn(true);
        assertTrue(stockDao.printStoreWithMaxProductsInCategory(""));
    }
}