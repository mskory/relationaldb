DROP SCHEMA IF EXISTS shops_db CASCADE;
CREATE SCHEMA shops_db AUTHORIZATION postgres;
--
-- DROP TABLE shops_db.stores;
-- DROP TABLE shops_db.products;
-- DROP TABLE shops_db.categories;
-- DROP TABLE shops_db.stock;

CREATE TABLE shops_db.stores
(
    id         SERIAL PRIMARY KEY,
    name       character varying COLLATE pg_catalog."default" NOT NULL,
    address    character varying COLLATE pg_catalog."default" NOT NULL
);

CREATE TABLE shops_db.categories
(
    id         SERIAL PRIMARY KEY,
    name       character varying COLLATE pg_catalog."default" NOT NULL,
    is_deleted BOOLEAN DEFAULT false
);

CREATE TABLE shops_db.products
(
    id           SERIAL PRIMARY KEY,
    name         character varying COLLATE pg_catalog."default" NOT NULL,
    category_id  int,
    FOREIGN KEY (category_id) REFERENCES shops_db.categories (id)
);

CREATE TABLE shops_db.stock
(
    product_id bigint,
    store_id   bigint,
    quantity   int,
    FOREIGN KEY (product_id) REFERENCES shops_db.products (id),
    FOREIGN KEY (store_id) REFERENCES shops_db.stores (id)
);
