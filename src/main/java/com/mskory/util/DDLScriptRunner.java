package com.mskory.util;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.Objects;
import java.util.Properties;

public abstract class DDLScriptRunner {
    private static final Logger logger = LoggerFactory.getLogger(DDLScriptRunner.class);

    public static void runScript(ConnectionPool connectionPool, Properties properties) {
        if (properties.getProperty("runScript").equals("true")) {
            String filename = properties.getProperty("ddlScriptFileName");
            logger.info("Run DDL script {} ", filename);
            Connection connection = connectionPool.getConnection();
            ScriptRunner scriptRunner = new ScriptRunner(connection);
            InputStreamReader reader = new InputStreamReader(Objects.requireNonNull(Thread.currentThread()
                    .getContextClassLoader().getResourceAsStream(filename), "Can't find file" + filename));
            scriptRunner.runScript(reader);
            connectionPool.releaseConnection(connection);
        } else {
            logger.info("DDL script runner was skipped");
        }
    }
}
