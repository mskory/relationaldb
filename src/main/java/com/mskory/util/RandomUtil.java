package com.mskory.util;

import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;

public abstract class RandomUtil {

    public static String getString(int maxLength) {
        return RandomStringUtils.randomAlphabetic(ThreadLocalRandom.current()
                .nextInt(1, maxLength + 1));
    }

    public static LocalDate getDate() {
        long minDay = LocalDate.EPOCH.toEpochDay();
        long maxDay = LocalDate.now().toEpochDay();
        long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
        return LocalDate.ofEpochDay(randomDay);
    }

    public static int getInt(int max) {
        return ThreadLocalRandom.current().nextInt(1, max + 1);
    }
    public static boolean getBoolean() {
        return ThreadLocalRandom.current().nextBoolean();
    }
}
