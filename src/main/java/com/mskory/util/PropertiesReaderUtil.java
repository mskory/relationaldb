package com.mskory.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public abstract class PropertiesReaderUtil {

    private static final String DEFAULT_PROPERTIES_FILE_NAME = "default.properties";

    public static Properties getProperties(String filename){
        Properties properties = new Properties();
        InputStream resourceAsStream = getResources(filename);
        if(resourceAsStream == null) resourceAsStream = getResources(DEFAULT_PROPERTIES_FILE_NAME);
        InputStreamReader reader = new InputStreamReader(resourceAsStream, StandardCharsets.UTF_8);
        try {
            properties.load(reader);
            reader.close();
            return properties;
        } catch (Exception e) {
            throw new RuntimeException("Can't load properties from file " + filename, e);
        }
    }

    public static InputStream getResources(String filename) {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
    }
}
