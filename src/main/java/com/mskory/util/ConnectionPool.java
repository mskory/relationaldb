package com.mskory.util;

import java.sql.Connection;

public interface ConnectionPool {

    Connection getConnection();
    boolean releaseConnection(Connection connection);
    void close();
    int getPoolSize();
}
