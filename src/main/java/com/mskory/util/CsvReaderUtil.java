package com.mskory.util;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.util.List;

public abstract class CsvReaderUtil {

    public static List<CSVRecord> read(String filename, String[] headers) {
        CSVFormat format = CSVFormat.RFC4180.builder()
                .setHeader(headers)
                .setSkipHeaderRecord(true)
                .build();
        try (Reader reader = new InputStreamReader(PropertiesReaderUtil.getResources(filename))){
            return format.parse(reader).getRecords();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
