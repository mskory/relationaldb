package com.mskory.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ConnectionPoolImpl implements ConnectionPool {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final Queue<Connection> inUseConnections;
    private final Queue<Connection> connectionPool;
    private final String user;
    private final String passwd;
    private final String jdbcDriver;
    private final String url;
    private final int poolSize;

    public ConnectionPoolImpl(Properties properties) {
        this.connectionPool = new ConcurrentLinkedQueue<>();
        this.inUseConnections = new ConcurrentLinkedQueue<>();
        this.poolSize = Integer.parseInt(properties.getProperty("poolSize"));
        this.user = properties.getProperty("user");
        this.passwd = properties.getProperty("password");
        this.jdbcDriver = properties.getProperty("jdbcDriver");
        this.url = properties.getProperty("url");
        createPool();
    }

    private void createPool() {
        try {
            Class.forName(jdbcDriver);
            for (int i = 0; i < poolSize; i++) {
                connectionPool.add(createConnection());
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Can't load JDBC driver for " + jdbcDriver, e);
        }
    }


    @Override
    public Connection getConnection() {
        if (connectionPool.size() == 0){
            throw new RuntimeException("Maximum pool size reached, no available connections!");
        }else{
            Connection connection = connectionPool.poll();
            inUseConnections.add(connection);
            return connection;
        }
    }

    @Override
    public boolean releaseConnection(Connection connection) {
        connectionPool.add(connection);
        return inUseConnections.remove(connection);
    }

    @Override
    public void close() {
        for (Connection connection : inUseConnections){
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException("Can't close connections", e);
            }
        }
        connectionPool.clear();
    }

    private Connection createConnection(){
        try {
            return DriverManager.getConnection(url, user, passwd);
        } catch (SQLException e) {
            throw new RuntimeException("Can't create connection", e);
        }
    }

    @Override
    public int getPoolSize() {
        return poolSize;
    }
}
