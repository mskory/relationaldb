package com.mskory;

import com.mskory.dao.*;
import com.mskory.models.Category;
import com.mskory.models.Product;
import com.mskory.models.Store;
import com.mskory.util.*;

import java.util.List;
import java.util.Properties;

public class App {
    public static final String CATEGORY_ARGS_NAME = "category";
    public static final String PROPERTIES_FILE_NAME = "app.properties";
    public static final String STORES_CSV_FILE_NAME = "stores.csv";
    public static final String CATEGORIES_CSV_FILE_NAME = "categories.csv";
    public static void main(String[] args) {
        System.setProperty("logback.configurationFile", "logback.xml");
        Properties properties = PropertiesReaderUtil.getProperties(PROPERTIES_FILE_NAME);
        ConnectionPool connectionPool = new ConnectionPoolImpl(properties);
        DDLScriptRunner.runScript(connectionPool, properties);

        Dao<Store> storeDao = new StoreDaoImpl(connectionPool);
        storeDao.addFromCSV(STORES_CSV_FILE_NAME);

        Dao<Category> categoryDao = new CategoryDaoImpl(connectionPool);
        categoryDao.addFromCSV(CATEGORIES_CSV_FILE_NAME);

        StockDaoImpl stockDaoImpl = new StockDaoImpl(connectionPool);

        List<Product> products = new ProductDaoImpl(connectionPool)
                .addRandomInParallel(categoryDao.getTotal(), properties);

        stockDaoImpl.addList(List.copyOf(products));
        stockDaoImpl.printStoreWithMaxProductsInCategory(System.getProperty(CATEGORY_ARGS_NAME));

        connectionPool.close();
    }
}
