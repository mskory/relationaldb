package com.mskory.dao;

import com.mskory.util.ConnectionPool;
import jakarta.validation.Validation;

import jakarta.validation.Validator;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.sql.*;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public interface Dao<T> {
    DecimalFormat decimalFormat = new DecimalFormat("#.##");
    StopWatch stopWatch = new StopWatch();
    Logger logger = LoggerFactory.getLogger(Dao.class);
    int BATCH = 1000;
    T create(T dtoObj);

    List<T> addList(List<T> dtoList);
    void addFromCSV(String filename);

    List<T> getAll();

    Optional<T> get(Long id);

    T update(T dtoObj);

    boolean delete(Long id);

    String getTableName();

    ConnectionPool getConnectionPool();

    default Integer getTotal(){
        ConnectionPool connectionPool = getConnectionPool();
        String query = String.format("SELECT COUNT(*) AS total FROM %s", this.getTableName());
        try {
            Connection connection = connectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                int result = resultSet.getInt("total");
                statement.close();
                connectionPool.releaseConnection(connection);
                return result;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Can't execute getTotal() method", e);
        }
        return null;
    }

    default Long getId(Statement statement) throws SQLException {
        ResultSet resultSet = statement.getGeneratedKeys();
        return resultSet.next() ? resultSet.getObject("id", Long.class) : null;
    }

    default void startStopWatch(){
        if (stopWatch.isStopped()){
            stopWatch.start();
        }else {
            stopWatch.reset();
            stopWatch.start();
        }
    }
    default void printResults(int totalRecords){
        long runTime = stopWatch.getTime(TimeUnit.MILLISECONDS);
        double rps = totalRecords / (runTime / 1000D);
        logger.info("Time to fill {} table with {} records is {} millis",
                this.getTableName(), totalRecords, runTime);
        logger.info("The operation was processed with {} rps", decimalFormat.format(rps));
    }
}
