package com.mskory.dao;

import com.mskory.models.Category;
import com.mskory.util.ConnectionPool;
import com.mskory.util.CsvReaderUtil;
import org.apache.commons.csv.CSVRecord;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class CategoryDaoImpl implements Dao<Category> {
    private static final String TABLE_NAME = "shops_db.categories";

    private final ConnectionPool connectionPool;

    public CategoryDaoImpl(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    @Override
    public Category create(Category category) {
        String query = String.format("INSERT INTO %s(name) values (?)", TABLE_NAME);
        try {
            Connection connection = connectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, category.getName());
            statement.executeUpdate();
            category.setId(getId(statement));
            statement.close();
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            throw new RuntimeException("Can't create category", e);
        }
        return category;
    }

    @Override
    public List<Category> addList(List<Category> categories) {
        String query = String.format("INSERT INTO %s(name) values (?)", TABLE_NAME);
        try {
            Connection connection = connectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            startStopWatch();
            for (int i = 0; i < categories.size(); i++) {
                statement.setString(1, categories.get(i).getName());
                statement.addBatch();
                if (i % Dao.BATCH == 0) {
                    statement.executeBatch();
                    statement.clearBatch();
                }
            }
            statement.executeBatch();
            double runTime = stopWatch.getTime(TimeUnit.MILLISECONDS);
            double rps = categories.size() / (runTime / 1000);
            logger.info("List of categories was added to DB. Number of elements: {}, time: {}ms RPS is {}",
                    categories.size(), runTime, decimalFormat.format(rps));
            statement.close();
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    @Override
    public void addFromCSV(String filename) {
        String[] headers = new String[]{"name"};
        List<CSVRecord> records = CsvReaderUtil.read(filename, headers);
        String query = "INSERT INTO shops_db.categories(name) VALUES (?);";
        try {
            Connection connection = connectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            connection.setAutoCommit(false);
            startStopWatch();
            for (CSVRecord record : records) {
                statement.setObject(1, record.get(headers[0]));
                statement.addBatch();
                if (records.indexOf(record) % Dao.BATCH == 0) {
                    statement.executeBatch();
                    connection.commit();
                }
            }
            statement.executeBatch();
            connection.commit();
            connection.setAutoCommit(true);
            printResults(records.size());
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Category> getAll() {
        List<Category> categories = new ArrayList<>();
        String query = String.format("SELECT * FROM %s WHERE is_deleted = false", TABLE_NAME);
        try {
            Connection connection = connectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long id = resultSet.getObject("id", Long.class);
                String name = resultSet.getString("name");
                categories.add(new Category().setName(name).setId(id));
            }
            statement.close();
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return categories;
    }

    @Override
    public Optional<Category> get(Long id) {
        String query = String.format("SELECT * FROM %s WHERE id = ? AND is_deleted = false", TABLE_NAME);
        try {
            Connection connection = connectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setObject(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                String name = resultSet.getString("name");
                return Optional.of(new Category().setName(name).setId(id));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return Optional.empty();
    }

    @Override
    public Category update(Category dtoObj) {
        String query = String.format("UPDATE %s SET name = ? WHERE id = ? AND is_deleted = false", TABLE_NAME);
        try {
            Connection connection = connectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setObject(1, dtoObj.getId());
            if (statement.executeUpdate() != 1) {
                logger.info("Can't update record. There is no record for {} in the database "
                        + "or record was deleted", dtoObj);
                statement.close();
                connectionPool.releaseConnection(connection);
                return null;
            }
            statement.close();
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return dtoObj;
    }

    @Override
    public boolean delete(Long id) {
        String query =
                String.format("UPDATE %s SET is_deleted = true WHERE id = ? AND is_deleted = false;", TABLE_NAME);
        Connection connection = connectionPool.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setObject(1, id);
            if (statement.executeUpdate() != 1) {
                logger.info("Can't delete record. There is no record with id {} in the database "
                        + "or the record was already deleted", id);
                return false;
            }
            statement.close();
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public ConnectionPool getConnectionPool() {
        return connectionPool;
    }
}
