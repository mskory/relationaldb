package com.mskory.dao;

import com.mskory.models.Store;
import com.mskory.util.ConnectionPool;
import com.mskory.util.CsvReaderUtil;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class StoreDaoImpl implements Dao<Store> {
    public static final String TABLE_NAME = "shops_db.stores";
    private final ConnectionPool connectionPool;

    public StoreDaoImpl(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    @Override
    public Store create(Store store) {
        String query = "INSERT INTO shops_db.stores(name, address) values (?, ?)";
        try {
            Connection connection = connectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, store.getName());
            statement.setString(1, store.getAddress());
            statement.executeUpdate();
            store.setId(getId(statement));
            statement.close();
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            throw new RuntimeException("Can't create store", e);
        }
        return store;
    }

    @Override
    public List<Store> addList(List<Store> stores) {
        String query = "INSERT INTO shops_db.stores(name, address) values (?, ?)";
        try {
            Connection connection = connectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            connection.setAutoCommit(false);
            for (int i = 0; i < stores.size(); i++) {
                Store store = stores.get(i);
                statement.setString(1, store.getName());
                statement.setString(2, store.getAddress());
                statement.addBatch();
                if (i % Dao.BATCH == 0) {
                    statement.executeBatch();
                    connection.commit();
                }
            }
            statement.executeBatch();
            connection.commit();
            connection.setAutoCommit(false);
            printResults(stores.size());
            statement.close();
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    @Override
    public void addFromCSV(String filename) {
        String[] headers = new String[]{"name", "address"};
        List<CSVRecord> records = CsvReaderUtil.read(filename, headers);
        String query = "INSERT INTO shops_db.stores(name, address) VALUES (?,?);";
        try {
            Connection connection = connectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            connection.setAutoCommit(false);
            startStopWatch();
            for (CSVRecord record : records) {
                statement.setObject(1, record.get(headers[0]));
                statement.setObject(2, record.get(headers[1]));
                statement.addBatch();
                int index = records.indexOf(record);
                if (index !=0 && index % Dao.BATCH == 0) {
                    statement.executeBatch();
                    connection.commit();
                }
            }
            statement.executeBatch();
            connection.commit();
            connection.setAutoCommit(true);
            printResults(records.size());
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Store> getAll() {
        return null;
    }

    @Override
    public Optional<Store> get(Long id) {
        return Optional.empty();
    }

    @Override
    public Store update(Store dtoObj) {
        return null;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public ConnectionPool getConnectionPool() {
        return connectionPool;
    }
}
