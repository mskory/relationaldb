package com.mskory.dao;

import com.mskory.models.Product;
import com.mskory.util.ConnectionPool;
import com.mskory.util.RandomUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class StockDaoImpl implements Dao<Product> {
    public static final String TABLE_NAME = "shops_db.stock";
    private final ConnectionPool connectionPool;

    public StockDaoImpl(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    @Override
    public List<Product> addList(List<Product> products) {
        String query = String.format("INSERT INTO %s (store_id, product_id, quantity) values (?, ?, ?)", TABLE_NAME);
        List<Long> storesIdes = getIdes(StoreDaoImpl.TABLE_NAME);
        try {
            Connection connection = connectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            connection.setAutoCommit(false);
            startStopWatch();
            for (Long storeId : storesIdes) {
                for (int i = 1; i < products.size() + 1; i++) {
                    Product product = products.get(i - 1);
                    statement.setObject(1, storeId);
                    statement.setObject(2, product.getId());
                    statement.setObject(3, RandomUtil.getInt(product.getAmount()));
                    statement.addBatch();
                    if (i % Dao.BATCH == 0) {
                        statement.executeBatch();
                        connection.commit();
                    }
                }
                statement.executeBatch();
                connection.commit();
            }
            printResults(products.size() * storesIdes.size());
            connection.setAutoCommit(true);
            String indexQuery = "CREATE INDEX stock_index ON shops_db.stock(product_id); " +
                    "CREATE INDEX product_index ON shops_db.products(category_id);" +
                    "CREATE INDEX category_idx ON shops_db.categories (id);";
            PreparedStatement indexStatement = connection.prepareStatement(indexQuery);
            indexStatement.execute();
            indexStatement.close();
            statement.close();
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return products;
    }

    public boolean printStoreWithMaxProductsInCategory(String categoryName) {
        String query = "WITH products_by_category AS (SELECT products.id " +
                "FROM shops_db.products " +
                "INNER JOIN shops_db.categories on categories.id = products.category_id " +
                "WHERE categories.name = ?), " +
                "stock_by_category AS (SELECT (SELECT concat(stores.name, ', ', stores.address) AS store " +
                "FROM shops_db.stores " +
                "WHERE store_id = stores.id), " +
                "sum(quantity) as total " +
                "FROM products_by_category " +
                "INNER JOIN shops_db.stock ON stock.product_id = products_by_category.id " +
                "GROUP BY store_id) " +
                "SELECT * " +
                "FROM stock_by_category " +
                "ORDER BY total DESC " +
                "LIMIT 1;";
        try {
            Connection connection = connectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, categoryName);
            startStopWatch();
            ResultSet resultSet = statement.executeQuery();
            logger.info("Time to execute search query is {} millis", stopWatch.getTime(TimeUnit.MILLISECONDS));
            if (resultSet.next()) {
                String store = resultSet.getString(1);
                String total = resultSet.getString(2);
                logger.info("{}. Категорія - {}. Кількість продуктів: {}",
                        store, categoryName, total);
                return true;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return false;
    }

    @Override
    public Product create(Product dtoObj) {
        return null;
    }

    @Override
    public void addFromCSV(String filename) {
    }

    @Override
    public List<Product> getAll() {
        return null;
    }

    @Override
    public Optional<Product> get(Long id) {
        return Optional.empty();
    }

    @Override
    public Product update(Product dtoObj) {
        return null;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public ConnectionPool getConnectionPool() {
        return connectionPool;
    }

    private List<Long> getIdes(String tableName) {
        List<Long> ides = new ArrayList<>();
        String query = String.format("SELECT id FROM %s", tableName);
        try {
            Connection connection = connectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                ides.add((long) resultSet.getInt("id"));
            }
            statement.close();
            connectionPool.releaseConnection(connection);
            return ides;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
