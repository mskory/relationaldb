package com.mskory.dao;

import com.mskory.models.Product;
import com.mskory.service.ProductDtoGenerator;
import com.mskory.util.ConnectionPool;

import java.sql.*;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ProductDaoImpl implements Dao<Product> {
    public static final String TABLE_NAME = "shops_db.products";
    private final ConnectionPool connectionPool;

    public ProductDaoImpl(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    @Override
    public List<Product> addList(List<Product> products) {
        String query = "INSERT INTO shops_db.products(name, category_id) values (?, ?)";
        try {
            Connection connection = connectionPool.getConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            startStopWatch();
            connection.setAutoCommit(false);
            for (int i = 0; i < products.size(); i++) {
                Product pro = products.get(i);
                statement.setString(1, pro.getName());
                statement.setObject(2, pro.getCategoryId());
                statement.addBatch();
                if (i != 0 && (i+1) % Dao.BATCH == 0) {
                    statement.executeBatch();
                    connection.commit();
                    setGeneratedKeys(products, statement, i);
                }
            }
            statement.executeBatch();
            connection.commit();
            setGeneratedKeys(products, statement, products.size() - (products.size() % Dao.BATCH));
            printResults(products.size());
            connection.setAutoCommit(true);
            statement.close();
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return products;
    }
    public List<Product> addRandomInParallel(int categoriesAmount, Properties properties ){
        int productsTotal = Integer.parseInt(properties.getProperty("productsAmount"));
        int productQuantity = Integer.parseInt(properties.getProperty("quantityPerProduct"));
        Queue<Product> products = new ConcurrentLinkedQueue<>();
        long startTime = System.currentTimeMillis();
        int poolSize = connectionPool.getPoolSize();
        ExecutorService executorService = Executors.newFixedThreadPool(poolSize);
        for (int i = 0; i < poolSize; i++) {
            executorService.submit(() -> products.addAll(
                    new ProductDaoImpl(connectionPool).addList(
                            new ProductDtoGenerator(categoriesAmount,productQuantity)
                                    .generateList(productsTotal / poolSize))));
        }
        executorService.shutdown();
        while (!executorService.isTerminated()){
            Thread.onSpinWait();
        }
        logger.info("Total time for adding {} products to the {} table is {} millis",
                products.size(), ProductDaoImpl.TABLE_NAME, System.currentTimeMillis() - startTime);
        return List.copyOf(products);
    }

    private void setGeneratedKeys(List<Product> products, PreparedStatement statement, int fromIndex) {
        try {
            ResultSet result = statement.getGeneratedKeys();
            int i = fromIndex == 0 ? 0 : fromIndex - BATCH + 1;
            while (result.next()) {
                products.get(i++).setId((long) result.getInt("id"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Product create(Product dtoObj) {
        return null;
    }

    @Override
    public void addFromCSV(String filename) {
    }

    @Override
    public List<Product> getAll() {
        return null;
    }

    @Override
    public Optional<Product> get(Long id) {
        return Optional.empty();
    }

    @Override
    public Product update(Product dtoObj) {
        return null;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public ConnectionPool getConnectionPool() {
        return connectionPool;
    }
}
