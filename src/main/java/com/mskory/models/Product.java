package com.mskory.models;

import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public class Product {
    private Long id;
    private Long categoryId;
    private String  name;

    private int amount;

    public Long getId() {
        return id;
    }

    public Product setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public Product setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    @Pattern(regexp = "\\w*[aA]+\\w*")
    @Size(min = 5, max = 20, message = "{category.name.invalid}")
    public String getName() {
        return name;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }

    public Product setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", name='" + name + '\'' +
                '}';
    }
}
