package com.mskory.service;

import com.mskory.models.Product;
import com.mskory.util.RandomUtil;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ProductDtoGenerator implements DtoGenerator<Product> {
    private final int numberOfCategories;
    private final int quantityPerProduct;

    public ProductDtoGenerator(int numberOfCategories, int quantityPerProduct) {
        this.quantityPerProduct = quantityPerProduct;
        this.numberOfCategories = numberOfCategories;
    }

    @Override
    public List<Product> generateList(int amount) {
        return Stream.generate(() -> new Product()
                        .setName(RandomUtil.getString(30))
                        .setCategoryId((long) RandomUtil.getInt(numberOfCategories))
                        .setAmount(quantityPerProduct))
                .parallel()
                .filter(product -> validator.validate(product).isEmpty())
                .limit(amount)
                .collect(Collectors.toList());
    }
}
