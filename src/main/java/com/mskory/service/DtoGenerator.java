package com.mskory.service;

import jakarta.validation.Validation;
import jakarta.validation.Validator;

import java.util.List;

public interface DtoGenerator <T> {
    Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    List<T> generateList(int amount);
}
